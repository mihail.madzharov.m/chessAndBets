import { Component } from '@angular/core';
import { User } from '../Models/users.model'
import { FormPoster } from '../services/form-poster.service'
import { NgForm } from '@angular/forms'

@Component({
  selector: 'home',
  styleUrls: ['./home.component.css'],
  templateUrl: './home.component.html',
})
export class HomeComponent {
  languages = ["English", "Not English", "Enough of the English"];
  user = new User("Mish", "Madzharov", 0);
  constructor(private formPoster: FormPoster) { }
  submitForm(form: NgForm) {

    this.formPoster.postEmployeeForm(this.user)
      .subscribe(
      data => console.log('success', data),
      err => console.log('err', err)
      )
  }
}
