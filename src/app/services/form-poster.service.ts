import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { User } from '../Models/users.model';
import 'rxjs/Rx'
import { Observable } from 'rxjs/Observable';

@Injectable()
export class FormPoster {

    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }
    private handleError(error: any) {
        console.error('post error', error)
        return Observable.throw(error.statusText);
    }

    constructor(private http: Http) {
    }

    postEmployeeForm(user: User): Observable<any> {
        let body = JSON.stringify(user);
        let headers = new Headers({ 'Content-type': 'application/json' })
        let options = new RequestOptions({ headers: headers });
        return this.http.post('http://localhost:30601/api/user', body, options).map(this.extractData).catch(this.handleError)
    }
}