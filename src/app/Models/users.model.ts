export class User {
    constructor(
        public firstName: string,
        public lastName: String,
        public amount: number
    ) { }
}